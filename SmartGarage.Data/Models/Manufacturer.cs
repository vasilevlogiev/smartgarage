﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Manufacturer
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Model> Models { get; set; } = new HashSet<Model>();

        public bool IsDeleted { get; set; }
    }
}
