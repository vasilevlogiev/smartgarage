﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SmartGarage.Data.Models
{
    public class Service
    {
        [Key]
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
