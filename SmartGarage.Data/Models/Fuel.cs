﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Fuel
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Type { get; set; }

        public bool IsDeleted { get; set; }
    }
}
