﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Vehicle
    {
        [Key]
        [RegularExpression("[A-HJ-NPR-Z0-9]{13}[0-9]{4}", ErrorMessage = "Invalid VIN format.")]
        public string VIN { get; set; }

        [Required]
        public string RegistrationNumber { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string RegistrationCountry { get; set; }

        [Range(0, 3000000)]
        public int Mileage { get; set; }

        public Guid ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }

        public Guid ModelId { get; set; }
        public Model Model { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public Guid OwnerId { get; set; }
        public User Owner { get; set; }

        public ICollection<Service> Services { get; set; } = new HashSet<Service>();

        public bool IsDeleted { get; set; }
    }
}