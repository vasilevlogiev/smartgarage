﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    class Gearbox
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Type { get; set; }

        public bool IsDeleted { get; set; }
    }
}
