﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Engine
    {
        [Key]
        public Guid Id { get; set; }

        [Range(10, 1000)]
        public int Horsepower { get; set; }

        [Range(500, 10000)]
        public int Size { get; set; }

        public Guid FuelId { get; set; }
        public Fuel Fuel { get; set; }

        public bool IsDeleted { get; set; }
    }
}
