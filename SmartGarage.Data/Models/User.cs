﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
