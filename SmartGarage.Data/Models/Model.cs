﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Model
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Guid ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }

        public bool IsDeleted { get; set; }
    }
}